import styled, {keyframes} from 'styled-components'


const slideIn = keyframes`
    0% {
        opacity: 0;
        transform: scale(30%);
    }
    100% {
        opacity: 1;
        transform: scale(100%);
    }
`
const spin = keyframes`
    0% {
        transform: rotate(0);
    }
    100% {
        transform: rotate(360);
    }
`
export const MenuContainer = styled.div`
    margin-left: 12px;
    position: absolute;
    box-sizing: border-box;
    left: 0;
    bottom: -70px;
    .menu-background {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        animation: ${slideIn} 0.05s ease-in forwards;
        background-color: rgba(0, 0, 0, 0.05);
    }

    #menu-toggler:checked ~ .menu-panel-wrapper,
    #menu-toggler:checked ~ .menu-background {
        display: block;
    }
    #menu-toggler:checked ~ .menu-toggle-trigger .menu-toggle-icon-show {
        display: inline;
    }
    #menu-toggler:checked ~ .menu-toggle-trigger .menu-toggle-icon-hide {
        display: none;
    }
`

export const MenuToggler = styled.label`
    font-weight: bold;
    color: var(--dark-color);
    position: relative;
    z-index: 10;
    .menu-toggle-icon-show {
        display: none;
    }
    span {
        border-radius: 50%;
        padding: 20px;
        background-color: rgba(208, 209, 210, 0.8);
    }
    cursor: pointer;
    :hover {
        color: orange;
    }
    :hover span {
        background-color: rgba(126, 127, 129, 0.8);
    }
`

export const MenuPanel = styled.div`
    display: none;
    width: 500px;
    overflow-y: scroll;
    height: 70vh;
    border-radius: 5px;
    position: absolute;
    box-shadow: 0 2px 3px 4px rgba(0,0,0,0.08);
    background-image: linear-gradient(to left, #f3c6b4 , #a6ebc8);
    top: calc(100% + 20px);
    z-index: 2;
    bottom: 0;
    left: 0px;
    transform-origin: 0 top;
    animation: 0.2s ${slideIn} ease-in forwards;
    ::-webkit-scrollbar {
        width: 5px;
    }
    ::-webkit-scrollbar-track {
        background: #f1f1f1;
    }
    ::-webkit-scrollbar-thumb {
        background: #458077;
        border-radius: 20px;
    }

    ::-webkit-scrollbar-thumb:hover {
        background: #5bbfce;
    }

    
    .menu-panel-container {
        padding: 0;
    }
    .menu-list-parent {
        margin-top: 60px;
        .menu-list {
            list-style: none;
            padding: 0;
            margin: 0;
            li {
                
                :hover {
                    background-color: #abeaad;
                }
            }
            .menu-item-link {
                width: 100%;
                padding: 20px;
                display: flex;
                align-items: center;
                text-decoration: none;
                justify-content: center;
                color: #3d91a8;
                :hover {
                    color: orange;
                }
            }
            .menu-item-image-wrapper {
                position: relative;
                height: 40px;
                width: 40px;
                color: inherit;
            }
            .menu-item-title {
                padding: 0 20px;
                font-weight: bold;
            }
        }
    }
    .menu-list-close {
        position: absolute;
        top: 0;
        right: 0;
        cursor: pointer;
        border-radius: 0 5px 0 5px;
        padding: 14px;
        background-color: #d2947d;
        color: #25955d;
        :hover {
            opacity: 0.7;
            color: #000;
        }
    }

    @media screen and (max-width:576px) {
        width: calc(100vw - 50px);
    }
`
