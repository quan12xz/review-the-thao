import { MenuContainer, MenuPanel, MenuToggler } from "./Menu.styles"
import { Container } from "react-bootstrap";
import MenuOpenIcon from '@mui/icons-material/MenuOpen';
import CloseIcon from '@mui/icons-material/Close';
import Link from 'next/link'
import Image from 'next/image'
import MenuIcon from '@mui/icons-material/Menu';

const list = [
    {
        url: "/images/icons/polo-fashion-svgrepo-com.svg",
        title: "Thời trang",
        pageUrl: "/category/fashion"
    },
    {
        url: "/images/icons/earrings-jewel-svgrepo-com.svg",
        title: "Trang sức",
        pageUrl: "/category/jewelry"
    },
    {
        url: "/images/icons/cosmetic.svg",
        title: "Mĩ phẩm",
        pageUrl: "/category/cosmetic"
    },
    {
        url: "/images/icons/laptop-svgrepo-com.svg",
        title: "Laptop",
        pageUrl: "/category/laptop"
    },
    {
        url: "/images/icons/phone-svgrepo-com.svg",
        title: "Điện thoại",
        pageUrl: "/category/phone"
    },
    {
        url: "/images/icons/brainstorming-svgrepo-com.svg",
        title: "Học tập",
        pageUrl: "/category/study"
    }
]
const Menu = () => {
  return (
    <MenuContainer>
        <input type="checkbox" id="menu-toggler" hidden/>
        <MenuToggler htmlFor="menu-toggler" className="menu-toggle-trigger">
            <span>
                <MenuOpenIcon className="menu-toggle-icon-show"/>
                <MenuIcon className="menu-toggle-icon-hide"/>
            </span>
        </MenuToggler>
        <label htmlFor="menu-toggler" className="menu-background"></label> 
        <MenuPanel className="menu-panel-wrapper">
            <Container className="menu-panel-container">
                <div className="menu-list-parent">
                    <ul className="menu-list">
                        {
                            list?.map((item, index) => {
                                return (
                                    <li key={index}>
                                        <Link href={item.pageUrl} className="menu-item-link">
                                            <div className="menu-item-image-wrapper">
                                                <Image src={item.url} alt={item.title} fill/>
                                            </div>
                                            <div className="menu-item-title">{item.title}</div>
                                        </Link>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
                <label htmlFor="menu-toggler" className="menu-list-close"><CloseIcon /></label>
            </Container>
        </MenuPanel>
    </MenuContainer>
  )
}

export default Menu