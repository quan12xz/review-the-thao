import styled from 'styled-components'

export const PostMenuContainer = styled.div`
    .menu-post-title{
        display: flex;
        h4 {
            padding: 0 20px;
            text-transform: uppercase;
            line-height: 1.2;
            color: var(--dark-color);
        }
        hr {
            flex-grow: 1;
            border-color: #aaa;
            align-self: flex-end;
        }
    }
`

export const PostMenuList = styled.ul`
    list-style: none;
    padding: 0 0 0 20px;
    margin:0;
`

export const PostMenuItem = styled.li`
    padding: 5px 0 5px 20px;
    margin: 0;
    box-sizing: border-box;
    border: none;
    :hover {
        box-shadow: -10px 0 2px 0px #dc7952;
    }
    a {
        text-decoration: none;
        color: var(--gray-color);
    }

    h2,h3 {
        margin:0;
        font-weight: bold;
        text-transform: capitalize;
    }
    h2 {
        font-size: var(--font-pc-medium);
    }
    h3 {
        font-size: var(--font-pc-normal);
        padding-left: 20px;
    }
    .actived {
        box-shadow: -10px 0 2px 0px #1c5438;
    }
`