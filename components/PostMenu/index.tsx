import React from 'react'
import { PostMenuListType } from '../../utils/Types'
import { PostMenuContainer, PostMenuItem, PostMenuList } from './PostMenu.styles'
import Link from 'next/link'

const PostMenu = ({postMenu}: {postMenu: PostMenuListType}) => {
  return (
    <PostMenuContainer>
        <div className='menu-post-title'>
          <h4>MỤC LỤC</h4>
          <hr className='filler-line' />
        </div>
        <PostMenuList>
            {
              postMenu?.map((item, index) => {
                return (
                  <PostMenuItem key={index}>
                    <Link href={`/#${item?.id}`}>
                    {
                      item?.type === 2 ?
                      <h2>{item?.text}</h2> :
                      <h3>{item?.text}</h3>
                    }
                    </Link>
                  </PostMenuItem>
                )
              })
            }
        </PostMenuList>
    </PostMenuContainer>
  )
}

export default PostMenu