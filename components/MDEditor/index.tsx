import { MDEditorContainer, MDEditorContent } from "./MDEditor.styles"
import Panel from "./Panel"
import CloseIcon from '@mui/icons-material/Close';

const MDEditor = () => {
  return (
    <MDEditorContainer>
        <MDEditorContent>
            <input type='text' placeholder="Tiêu đề" className="title-input"/>
            <div className="tag-container">
                <div className="tag-selected-container">
                    <span className="tag-selected">
                        <span className="tag-selected_title">PHP</span>
                        <span className="tag-selected_remove"><CloseIcon /></span>
                    </span>
                </div>
                <input type='text' placeholder="Tiêu đề" className="tag-input"/>
            </div>
            <Panel />

        </MDEditorContent>
    </MDEditorContainer>
  )
}

export default MDEditor