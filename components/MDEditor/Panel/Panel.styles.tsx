import styled from 'styled-components'


export const PanelContainer = styled.div`
    border: 1px solid #ccc;
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    position: absolute;
    bottom: 10px;
    left: 10px;
    right: 10px;
    height: 82vh;
`

export const PanelContent = styled.div`
    flex-grow: 1;
    height: 100%;
    outline: none;
    font-size: var(--font-pc-normal);
    padding: 10px;
`

export const PanelToolbar = styled.div`
    display: flex;
    flex-basis: auto;
    align-items: center;
    flex-wrap: wrap;
    border-bottom: 1px solid #ccc;
    background-color: #eee;
    .icon-splitter {
        border-top: 30px solid var(--gray-color);
        margin: 0 5px;
        width: 1px;
    }
    .icon-container {
        width: 30px;
        height: 30px;
        text-align: center;
        align-content: center;
        cursor: pointer;
        :hover {
            background-color: #ccc;
        }
    }
`