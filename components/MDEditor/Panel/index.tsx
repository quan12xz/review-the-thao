import React, { useState } from 'react'
import { PanelContainer, PanelContent, PanelToolbar } from './Panel.styles'
import FormatBoldIcon from '@mui/icons-material/FormatBold';
import FormatItalicIcon from '@mui/icons-material/FormatItalic';
import StrikethroughSIcon from '@mui/icons-material/StrikethroughS';
import CodeIcon from '@mui/icons-material/Code';
import {FaHeading} from 'react-icons/fa'
import FormatQuoteIcon from '@mui/icons-material/FormatQuote';
import ListIcon from '@mui/icons-material/List';
import FormatListNumberedIcon from '@mui/icons-material/FormatListNumbered';
import TableViewIcon from '@mui/icons-material/TableView';
import HorizontalRuleIcon from '@mui/icons-material/HorizontalRule';
import LinkIcon from '@mui/icons-material/Link';
import ImageIcon from '@mui/icons-material/Image';
import IntegrationInstructionsIcon from '@mui/icons-material/IntegrationInstructions';
import FormatAlignLeftIcon from '@mui/icons-material/FormatAlignLeft';
import FormatAlignCenterIcon from '@mui/icons-material/FormatAlignCenter';
import FormatAlignRightIcon from '@mui/icons-material/FormatAlignRight';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import VerticalSplitIcon from '@mui/icons-material/VerticalSplit';
import FullscreenExitIcon from '@mui/icons-material/FullscreenExit';
import InsertEmoticonIcon from '@mui/icons-material/InsertEmoticon';
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
const Panel = () => {
  const [content, setContent] = useState('')
  const contentChangeHandler = (e: any) => {
    setContent(e.target.value)
  }
  
  return (
    <PanelContainer>
        <PanelToolbar>
          <span className='icon-container'>
            <FormatBoldIcon />
          </span>
          <span className='icon-container'>
            <FormatItalicIcon />
          </span>
          <span className='icon-container'>
            <StrikethroughSIcon />
          </span>
          <span className='icon-splitter'></span>
          <span className='icon-container'>
            <FaHeading />
          </span>
          <span className='icon-container'>
            <CodeIcon />
          </span>
          <span className='icon-container'>
            <FormatQuoteIcon />
          </span>
          <span className='icon-container'>
            <ListIcon />
          </span>
          <span className='icon-container'>
            <FormatListNumberedIcon />
          </span>
          <span className='icon-splitter'></span>
          <span className='icon-container'>
            <TableViewIcon />
          </span>
          <span className='icon-container'>
            <HorizontalRuleIcon />
          </span>
          <span className='icon-container'>
            <LinkIcon />
          </span>
          <span className='icon-container'>
            <ImageIcon />
          </span>
          <span className='icon-container'>
            <IntegrationInstructionsIcon />
          </span>
          <span className='icon-splitter'></span>
          <span className='icon-container'>
            <FormatAlignLeftIcon />
          </span>
          <span className='icon-container'>
            <FormatAlignCenterIcon />
          </span>
          <span className='icon-container'>
            <FormatAlignRightIcon />
          </span>
          <span className='icon-splitter'></span>
          <span className='icon-container'>
            <RemoveRedEyeIcon />
          </span>
          <span className='icon-container'>
            <VerticalSplitIcon />
          </span>
          <span className='icon-container'>
            <FullscreenExitIcon />
          </span>
          <span className='icon-splitter'></span>
          <span className='icon-container'>
            <InsertEmoticonIcon />
          </span>
          <span className='icon-container'>
            <QuestionMarkIcon />
          </span>
        </PanelToolbar>
      <PanelContent contentEditable onKeyUp={contentChangeHandler}/>
    </PanelContainer>
  )
}

export default Panel