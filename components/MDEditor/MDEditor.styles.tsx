import styled from 'styled-components'

export const MDEditorContainer = styled.div`
    width: 100vw;
    height: 100vh;
`

export const MDEditorContent = styled.div`
    display: flex;
    flex-direction: column;
    padding: 10px;
    position: relative;
    width: 100vw;
    height: 100vh;
    
    .title-input {
        width: 100%;
        border: 1px solid #999;
        font-size: 1.2rem;
        border-radius: 5px;
        padding: 5px 15px;
    }
    .tag-container {
        width: 100%;
        font-size: 1.2rem;
        margin: 30px 0;
        border: 1px solid #999;
        border-radius: 5px;
        display: flex;
        .tag-selected-container {
            padding: 5px 20px 5px 10px;
            .tag-selected {
                border: 1px solid rgba(0, 0, 0, 0.09);
                border-radius: 5px;
                padding: 3px 10px;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .tag-selected_title {
                margin-right: 10px;
            }
            .tag-selected_title,.tag-selected_remove {
                font-size: var(--font-pc-small);
                font-weight: bold;
                color: var(--gray-color);
            }
        }
        
        .tag-input{
            flex-grow: 1;
            border: none;
            outline: none;
        }
    }
`