import Head from "next/head";
import React, { useEffect, useState } from "react";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import NavDropdown from "react-bootstrap/NavDropdown";
import Offcanvas from "react-bootstrap/Offcanvas";
import SportsGymnasticsIcon from "@mui/icons-material/SportsGymnastics";
import SearchIcon from '@mui/icons-material/Search';
import CloseIcon from '@mui/icons-material/Close';
import Link from "next/link";
import { Footer, NavWrapper, SearchButton, SearchWrapper } from "./Layout.styles";
import Menu from "../Menu";

const Layout = ({ children }: React.PropsWithChildren) => {
    const [isSearching, setIsSearching] = useState(false)

    const handleSearchToggle = () => {
        setIsSearching((isSearching) => !isSearching )
    }
	return (
		<div className="wrapper">
			<Head>
				<meta charSet="UTF-8" />
				<meta
					name="viewport"
					content="width=device-width, initial-scale=1.0"
				></meta>
				<title>review</title>
			</Head>
			<NavWrapper>
				<Navbar bg="light" expand={"md"} className="mb-3" style={{minHeight: "60px"}}>
					<Container className="position-relative">
						<Link href="/" className="home-link">
							<SportsGymnasticsIcon />
							Trang chủ
						</Link>

						<Navbar.Toggle
							aria-controls={`offcanvasNavbar-expand-${"md"}`}
						/>
						<Navbar.Offcanvas
							id={`offcanvasNavbar-expand-${"md"}`}
							aria-labelledby={`offcanvasNavbarLabel-expand-${"md"}`}
							placement="end"
						>
							<Offcanvas.Header closeButton>
								<Offcanvas.Title
									id={`offcanvasNavbarLabel-expand-${"md"}`}
								>
									<SportsGymnasticsIcon />
								</Offcanvas.Title>
							</Offcanvas.Header>
							<Offcanvas.Body>
								<Nav className="justify-content-center flex-grow-1 pe-3 fw-bolder align-items-center">
                                {!isSearching && 
									<>
                                    <Link href={"/posts"} className="nav-link" passHref>Posts</Link>
									<Link href={"/news"} className="nav-link" passHref>News</Link>
									<Link href={"/about"} className="nav-link" passHref>About</Link>
                                    </>
                                }
								</Nav>
                                <SearchWrapper>
                                    {
                                        isSearching ?
                                        <div className="search-box-wrapper">
                                            <input type={"text"} placeholder="Tìm kiếm" className="search-box-input" />
                                            <div className="search-box-button"><SearchIcon/></div>
                                        </div> :
                                        <div onClick={handleSearchToggle} className="search-box-label">Tìm kiếm</div>
                                    }
                                    <SearchButton onClick={handleSearchToggle}>
                                        {isSearching ? <CloseIcon /> : <SearchIcon/>}
                                    </SearchButton>
                                </SearchWrapper>
							</Offcanvas.Body>
						</Navbar.Offcanvas>
						<Menu />
					</Container>
				</Navbar>
			</NavWrapper>
			<main>{children}</main>
			<Footer>
				
			</Footer>
		</div>
	);
};

export default Layout;
