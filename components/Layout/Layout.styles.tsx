import styled from 'styled-components'

export const NavWrapper = styled.div`
    height: 60px;
	width: 100%;
	box-shadow: 0 2px 2px 3px rgba(0, 0, 0, 0.07);
    font-weight: 400;
    position: sticky;
    top: 0;
    z-index: 1000;
`;

export const SearchWrapper = styled.div`
    display: flex;
    justify-items: center;
    align-items: center;
    position: relative;
    .search-box-wrapper {
        border: 1px solid #eee;
        display: flex;
        justify-items: center;
        align-items: center;
    }
    .search-box-button {
        padding: 10px;
        background-color: #555;
        :hover{
            background-color: #777;
            cursor: pointer;
        }
    }
    .search-box-input {
        border: none;
        width: 400px;
        outline: none;
        padding: 10px;
        font-size: 1rem;
    }
    .search-box-label {
        color: #333;
        font-weight: 500;
        :hover {
            cursor: pointer;
            color: #222;
        }
    }
`
export const SearchButton = styled.div`
    padding: 10px;
    border-radius: 50%;
    margin-left: 10px;
    :hover {
        cursor: pointer;
        background-color: #eee;
    }
`

export const Footer = styled.div``