import styled from 'styled-components'

export const ListCategoryContainer = styled.div`
    margin-top: 30px;
    border-top: 1px solid rgba(0, 0, 0, 0.1);

    a:link {
        text-decoration: none;
    }
`

export const ItemCategoryContainer = styled.div`
    display: block;
    text-align: center;
    width: 100%;
    padding: 20px 10px;
    margin: 20px 0;
    border-radius: 2px;
    background-color: #eee;
    color: var(--dark-color);
    text-decoration: none;

    span {
        padding: 10px;
        font-size: var(--font-pc-normal);
        font-weight: bold;
        text-decoration: none ;
    }
    :hover {
        cursor: pointer;
        color: #000;
        background-color: #ccc;
    }
`


export const IconWrapper = styled.span`
    display: block;
    width: 100%;
    height: 120px;
    position: relative; 
`
