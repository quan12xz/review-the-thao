import React from 'react'
import { IconWrapper, ItemCategoryContainer, ListCategoryContainer } from './ListCategory.styles'
import Image from 'next/image'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Link from 'next/link'

const list = [
    {
        url: "/images/icons/polo-fashion-svgrepo-com.svg",
        title: "Thời trang",
        pageUrl: "/category/fashion"
    },
    {
        url: "/images/icons/earrings-jewel-svgrepo-com.svg",
        title: "Trang sức",
        pageUrl: "/category/jewelry"
    },
    {
        url: "/images/icons/cosmetic.svg",
        title: "Mĩ phẩm",
        pageUrl: "/category/cosmetic"
    },
    {
        url: "/images/icons/laptop-svgrepo-com.svg",
        title: "Laptop",
        pageUrl: "/category/laptop"
    },
    {
        url: "/images/icons/phone-svgrepo-com.svg",
        title: "Điện thoại",
        pageUrl: "/category/phone"
    },
    {
        url: "/images/icons/brainstorming-svgrepo-com.svg",
        title: "Học tập",
        pageUrl: "/category/study"
    }
]
const ListCategory = () => {
  return (
    <ListCategoryContainer>
        <Row>
        {
            list?.map((item, index) => (
                <Col xl={3} md={4} sm={6} key={index}>
                    <Link href={item.pageUrl} passHref>
                        <ItemCategoryContainer key={index}>
                            <IconWrapper>
                                <Image src={item.url} fill alt='image category'/>
                            </IconWrapper>
                            <span>{item.title}</span>
                        </ItemCategoryContainer>
                    </Link>
                </Col>
            ))
        }
        </Row>
    </ListCategoryContainer>
  )
}

export default ListCategory