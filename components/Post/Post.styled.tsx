import styled from 'styled-components'

export const PostContainer = styled.div`
    max-width: 400px;
    max-height: 300px;
    display: flex;
    overflow: hidden;
    position: relative;

    @media (max-width: 1200px) {
        max-width: 100%;
        margin: 10px;
    }
`

export const PostImageContainer = styled.div`
    position: relative;
    height: 100px;
    min-width: 150px;
    margin: 10px;
`

export const PostContentContainer = styled.div`
    margin: 10px 0;
    display: flex;
    flex-direction: column;
    color: var(--dark-color);
    flex-grow: 1;
    word-wrap: normal;
    text-decoration: none;
    overflow: hidden;
    .post-title {
        color: inherit;
        font-size: var(--font-pc-normal);
        text-decoration: inherit;
        :hover {
            text-decoration: underline;
        }

        @media (max-width: 768px) {
            font-size: var(--font-mobile-normal);
        }
    }
    .post-type {
        display: block;
        font-size: var(--font-pc-small);
        text-decoration: inherit;
        text-align: end;
        color: var(--gray-color);
        position: absolute;
        bottom: 10px;
        right: 0;
        :hover {
            text-decoration: underline;
        }
        @media (min-width: 768px) {
            padding-right: 4px;
        }
    }
`