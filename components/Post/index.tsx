import { PostContainer, PostContentContainer, PostImageContainer } from "./Post.styled"
import { PostType } from "../../utils/Types"
import Image from 'next/image'
import Link from 'next/link'

const Post = ({post}: {post:PostType}) => {
  return (
    <PostContainer>
        <PostImageContainer>
            <Image src={post.imgUrl} fill alt="post's image"/>
        </PostImageContainer>
        <PostContentContainer>
            <Link href={`/post/${post.id}`} className="post-title">{post.postTitle}</Link>
            <Link href={`/post/category/${post.postType}`} className="post-type">{post.postType}</Link>
        </PostContentContainer>
    </PostContainer>
  )
}

export default Post