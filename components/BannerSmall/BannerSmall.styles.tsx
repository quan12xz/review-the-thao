import styled from 'styled-components'

export const BannerContainer = styled.div``

export const BannerImageContainer = styled.div`
    position: relative;
    width: 100%;
    height: 300px;
    border-radius: 5px;
    overflow: hidden;
`
