import React from 'react'
import { BannerContainer, BannerImageContainer } from './BannerSmall.styles'
import Image from 'next/image'

type PropsType = {
    bannerSrc: string,

}
const BannerSmall = ({bannerSrc}: PropsType) => {
  return (
    <BannerContainer>
        <BannerImageContainer>
            <Image src={bannerSrc} alt="banner image" fill/>
        </BannerImageContainer>
    </BannerContainer>
  )
}

export default BannerSmall