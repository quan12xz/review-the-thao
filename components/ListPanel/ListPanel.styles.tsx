import styled from 'styled-components'

export const ListPanelContainer = styled.div`

`
export const ListPanelItemContainer = styled.div`
    background-image: linear-gradient(to left, #f8ede8 , #cff0e0);
    margin: 10px 0;
    border-radius: 10px;
    height: 300px;
    position: relative;
    transition: all 0.2s;
    
    :hover {
        color: #000;
        transform: translateY(-2px);
        background-image: linear-gradient(to left, #f3c6b4 , #a6ebc8);
        :before {
            content: "";
            z-index: -1;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background: linear-gradient(-45deg, #dc7952 0%, #1c5438 100% );
            transform: translate3d(0px, 20px, 0) scale(0.95);
            filter: blur(20px);
            opacity: var(0.7);
            transition: opacity 0.3s;
            border-radius: inherit;
        }
        ::after {
            content: "";
            z-index: -1;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background: inherit;
            border-radius: inherit;
        }
    }
`

export const ListPanelImageWrapper = styled.div`
    position: relative;
    height: 150px;
    object-fit: cover;
    border-radius: 10px 10px 0 0;
    overflow: hidden;
`

export const ListPanelContentWrapper = styled.div`
    display: flex;
    flex-direction: column;
    padding: 10px;
    max-height: 120px;
    overflow: hidden;
    .post-extra-info {
        display: flex;
        
        .post-extra-category {
            text-overflow: ellipsis;
            white-space: nowrap;
            max-width: 150px;
            flex-grow: 2;
            overflow: hidden;
        }
        .post-extra-time {
            flex-grow: 1.5;
            text-align: end;
        }

    }
    .post-main-title {
        font-size: var(--font-pc-normal);
        margin-top: 10px;
        font-weight: 600;
    }
`