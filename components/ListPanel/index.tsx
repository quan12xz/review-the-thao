import { EmotionVotedList, PostType } from "../../utils/Types"
import { ListPanelContainer, ListPanelContentWrapper, ListPanelImageWrapper, ListPanelItemContainer } from "./ListPanel.styles"
import Image from 'next/image'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Emotion from "../Emotion"
import {useState, useEffect} from 'react'
import { Button, Spinner } from "react-bootstrap"
import { ListFamousPost } from "../../services/fetch/FamousPost"
import Link from "next/link"

const emotionVotedList: EmotionVotedList = [
    ["like", 19],
    ["love", 10],
    ["angry", 1],
    ["surprise", 3]
]
const ListPanel = () => {
    const [emotions, setEmotions] = useState(emotionVotedList)
    const [posts, setPosts] = useState<Array<PostType>>([])
    const [loadingMore, setLoadingMore] = useState(false)
    const [page, setPage] = useState(1)
    const fetchListFamous = async () => {
        setLoadingMore(true)
      const data = await ListFamousPost(1)
      setPosts((posts) => {
        if(data && data.length > 0) {
          return [...posts, ...data]
        }
        return posts
      })
      setLoadingMore(false)
    }
    useEffect(() => {
        fetchListFamous()
      }, [page])
    const voteHandler = (type: string):void => {
        setEmotions((emotions) => {
            for( let i = 0; i < emotions.length ; i++) {
                if(emotions[i][0] === type) {
                    emotions[i][1]++
                    break
                }
            }
            return [...emotions]
        })
    }
    const handleLoadingMore = () => {
        setPage((page)=> {
            return page +1 
        })
    }
  return (
    <ListPanelContainer>
        <Row className="mt-4 top-splitter p-4">
            <Col>
                <h2 className="home-heading">Tin tức nổi bật</h2>
            </Col>
        </Row>
        <Row>
        {
            posts?.map((post: PostType, index: number) => {
                var bigSize = false
                if (index === 0 || (index + 1) % 6 === 0) {
                    bigSize = true
                }
                return (
                    <Col key={index} lg={bigSize ? 6 : 3} md={bigSize ? 8 : 4}>
                        <Link href={"/posts/1"} className="text-decoration-none" style={{color: "inherit"}}>
                            <ListPanelItemContainer>
                                <ListPanelImageWrapper>
                                    <Image src={post.imgUrl} alt={post.imgAlt?? "post image"} fill objectFit="cover"/>
                                </ListPanelImageWrapper>
                                <ListPanelContentWrapper>
                                    <div className="post-extra-info">
                                        <span className="post-extra-category">{post.postType}</span>
                                        <span className="post-extra-time">{post.postTime ?? "12 giờ trước"}</span>
                                    </div>
                                    <span className="post-main-title">{post.postTitle}</span>
                                </ListPanelContentWrapper>
                                <Emotion className="emotion" emotionVotedList={emotions} voteHandler={voteHandler} voted="love"/>
                            </ListPanelItemContainer>
                        </Link>
                    </Col>
                )
            })
        }
        </Row>
        <Row className="mt-4">
            <Col className="text-center">
            {
                loadingMore ? 
                <Spinner animation="border" /> 
                :
                <Button className="p-3" onClick={handleLoadingMore}>Xem tiếp</Button>
            }
            </Col>
        </Row>
    </ListPanelContainer>
  )
}

export default ListPanel