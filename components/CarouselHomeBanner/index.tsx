import Carousel from 'react-bootstrap/Carousel'
import Image from 'next/image'
import homeStyle from "../../styles/Home.module.css"
import { Banner } from '../../utils/Types'

const CarouselHomeBanner = ({listItem}: {listItem: Array<Banner>}) => {
  return (
    <Carousel fade>
        {
            listItem?.map((item, index) => (
                <Carousel.Item className={homeStyle.carouselItem} key={index}>
                    <Image
                        className="d-block "
                        src={item.imgUrl}
                        alt="First slide"
                        fill
                        sizes="(max-width: 768px) 100vw,
                        (max-width: 1200px) 50vw,
                        33vw"
                    />
                </Carousel.Item>
            ))
        }
    </Carousel>
  )
}

export default CarouselHomeBanner