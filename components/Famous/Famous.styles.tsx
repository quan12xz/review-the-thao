import styled from 'styled-components'

export const FamousContainer = styled.div`
    background-color: #eee;
    
`
    
export const FamousImageContainer = styled.div`
    position: relative;
    height: 500px;

`

export const FamousContent = styled.div`
    width: 100%;
    max-height: 200px;
    overflow: hidden;
    padding: 10px;
    font-weight: 600;
    color: var(--dark-color);
    .famous-type {
        font-size: var(--font-pc-small);
        @media (max-width: 768px){
            font-size: var(--font-mobile-small);
        }
    }
    .famous-title {
        font-size: var(--font-pc-medium);
        @media (max-width: 768px){
            font-size: var(--font-mobile-medium);
        }
    }
`