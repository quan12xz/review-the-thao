import {FamousContainer, FamousContent, FamousImageContainer} from './Famous.styles'
import Image from 'next/image'
import { PostType } from '../../utils/Types'
const Famous = ({post}: {post:PostType} ) => {
  return (
    <FamousContainer>
        <FamousImageContainer>
            <Image src={post.imgUrl} alt="most famous image" fill />
        </FamousImageContainer>
        <FamousContent>
            <div className='famous-type'>{post.postType}</div>
            <div className='famous-title'>{post.postTitle}</div>
        </FamousContent>
    </FamousContainer>
  )
}

export default Famous