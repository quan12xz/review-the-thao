import styled from 'styled-components'

export const EmotionContainer = styled.div`
    height: 30px;
    padding: 0 10px;
    position: absolute;
    bottom: 10px;
    display: flex;
    flex-direction: row;
    align-items: center;
    
`
export const EmotionToggler = styled.span`
    position: relative;

    :before {
        content: "";
        position: absolute;
        width: 50px;
        height: 20px;
        background-color: transparent;
        top: -15px;
    }
    :hover ~ .emotion-show {
        display: flex;
    }
`

export const EmotionShow = styled.div`

    position: absolute;
    display: none;
    top: -50px;
    width: 250px;
    height: 40px;
    justify-content: space-around;
    background-color: #ddd;
    border-radius: 5px;
    .vote-number {
        font-size: var(--font-pc-normal);
        padding: 0 2px;
    }
    :hover {
        display: flex;
    }
`

export const EmotionItemContainer = styled.div`
    position: relative;
    height: 40px;
    width: 40px;
    padding: 0 5px;
    margin: 0 5px;
    :hover {
        object-fit: cover;
        height: 50px;
        width: 50px;
        cursor: pointer;
    }
`
export const EmotionVotedContainer = styled.div`
    display: flex;
    align-items: center;
    height: 20px;
    width: fit-content;
    margin-left: 10px;
    
    
    .emotion-voted {
        height: 20px;
        width: 20px;
        padding: 0;
        margin: 0;
        position: relative;
        object-fit: contain;
        z-index: 10;
        :hover {

        }
    }
    .emotion-voted:nth-child(2) {
        transform: translateX(-8px);
        z-index: 9;
        height: 20px;
        width: 20px;
    }
    .emotion-voted:nth-child(3) {
        transform: translateX(-16px);
        z-index: 8;
        height: 20px;
        width: 20px;
    }
    .emotion-voted:nth-child(4) {
        transform: translateX(-26px);
        z-index: 7;
        height: 20px;
        width: 20px;
    }
`
export const EmotionVoted = styled.div`
    position: relative;
    height: 20px;
    width: 20px;
    :before {
        content: "";
        position: absolute;
        width: 50px;
        height: 20px;
        background-color: transparent;
        top: -15px;
    }
    :hover ~ .emotion-show {
        display: flex;
    }
`
