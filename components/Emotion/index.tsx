import React, { useEffect, useState } from 'react'
import { EmotionContainer, EmotionItemContainer, EmotionShow, EmotionToggler, EmotionVoted, EmotionVotedContainer } from './Emotion.styles'
import Image from 'next/image'
import AddReactionIcon from '@mui/icons-material/AddReaction';
import { EmotionVotedList } from '../../utils/Types';
import { CountEmotions, SortEmotionList } from '../../services/common/Common';
const emotionIcons = [
    {
        src: "/images/emotions/like.png",
        type: "like"
    },
    {
        src: "/images/emotions/love.png",
        type: "love",
    },
    {
        src: "/images/emotions/wow.png",
        type: "surprise"
    },
    {
        src: "/images/emotions/angry.png",
        type: "angry",
    }
]

type VoteHandlerType = (type: string) => void
const Emotion = ({emotionVotedList, voted, voteHandler, className}: {emotionVotedList: EmotionVotedList, voted: string, voteHandler: VoteHandlerType, className?: string}) => {
    const [emotions, setEmotions] = useState(emotionVotedList)
    const [countEmotions, setCountEmotions] = useState(0)
    useEffect(() => {
        const sortedList = SortEmotionList(emotions)
        setEmotions(sortedList)
        const count = CountEmotions(emotions)
        setCountEmotions(count)
    }, [emotionVotedList])

    const showVoted = () => {
        for (let index = 0; index < emotionIcons.length; index++) {
            if(emotionIcons[index].type === voted) {
                return <Image key={index} src={emotionIcons[index].src} alt={emotionIcons[index].type} fill/>
            }
        }
    }
  return (
    <EmotionContainer>
        {
        voted === "" ? 
            <EmotionToggler>
                <AddReactionIcon />
            </EmotionToggler>
            :
            <EmotionVoted>
                {
                    showVoted()
                }
            </EmotionVoted>
        }

        <EmotionShow className='emotion-show'>
        {
            emotionIcons?.map((item, index) => {
                return (
                    <EmotionItemContainer key={index} onClick={(e) => voteHandler(item.type)}>
                        <Image src={item.src} alt={item.type} fill/>
                    </EmotionItemContainer>
                )
            })
        }
        </EmotionShow>
        <EmotionVotedContainer>
        {
            voted ==="" && emotions?.map((item, index) => {
                if(item[0] === "like") {
                    return <EmotionItemContainer className='emotion-voted' key={index}>
                                <Image src={emotionIcons[0].src} alt={emotionIcons[0].type} fill/>
                            </EmotionItemContainer> 
                } else if (item[0] === "love") {
                    return <EmotionItemContainer className='emotion-voted' key={index}>
                                <Image src={emotionIcons[1].src} alt={emotionIcons[1].type} fill/>
                            </EmotionItemContainer>
                } else if (item[0] === "surprise") {
                    return <EmotionItemContainer className='emotion-voted' key={index}>
                                <Image src={emotionIcons[2].src} alt={emotionIcons[2].type} fill/>
                            </EmotionItemContainer>
                }
                return <EmotionItemContainer className='emotion-voted' key={index}>
                            <Image src={emotionIcons[3].src} alt={emotionIcons[3].type} fill/>
                        </EmotionItemContainer>
            })
        }
        {
            <span>{countEmotions}</span>
        }
        </EmotionVotedContainer>
    </EmotionContainer>
  )
}

export default Emotion