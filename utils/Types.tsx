export type PostType = {
    id: number,
    imgUrl: string,
    imgAlt?: string,
    postType: string,
    postTitle: string,
    postTime?:number,
    
}

export type IconType = {
    url: string,
    title?: string,
}

export type Banner = {
    link: string,
    imgUrl: string,
}

export type EmotionVotedList = Array<[string, number]>

export type PostMenuItemType = {id: string, type: number,  text: string}
export type PostMenuListType = [
    PostMenuItemType?
]