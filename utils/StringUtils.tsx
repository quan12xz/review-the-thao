import { PostMenuListType } from "./Types"

/**
 * Convert post content to menu of heading
 * @param content content of post
 * @returns array Containt object {id, text}
 */
export const getMenuPost = (content: string): PostMenuListType => {
    const pattern = /(<h[23].*?'>).*?<\/h[23]>/g
    const found = content.match(pattern)
    var formatted: PostMenuListType = []
    if(found) {
      for(var i = 0; i < found.length ; i++) {
        let regExID = /(?<=h[23].*?id=').*?(?=')/g
        let regExType = /<h2/g
        let regExText = /(?<=h[23].*?>).*?(?=<\/h[23]>)/g
        let id = found[i].match(regExID)
        let type = found[i].match(regExType) ? 2: 3
        let text = found[i].match(regExText)
        if(id && text) {
          formatted.push({id: id[0], type, text: text[0]})
        }
      }
    }
    return formatted
}

export const appendFormat = (firstString: String, type: String) => {
  var result = ''
  switch (type) {
    case 'BOLD':
        result += firstString + '****'
      break;
  
    default:
      break;
  }
  return result
}