import React from 'react'
import { Container } from 'react-bootstrap'
import BannerSmall from '../../components/BannerSmall'
import Layout from '../../components/Layout'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import styles from './Post.module.css'
import { getMenuPost } from '../../utils/StringUtils'
import PostMenu from '../../components/PostMenu'
import { PostMenuListType } from '../../utils/Types'
import axios from 'axios'
import matter from 'gray-matter'
import {serialize} from 'next-mdx-remote/serialize'
import {MDXRemote} from 'next-mdx-remote'

const Post = ({arrMenu, posts, source}: {arrMenu: PostMenuListType, posts:any, source: any}) => {
  
  return (
    <Layout>
      <Container>
        <BannerSmall bannerSrc="/images/2015-11-11-global-shopping0-16050036844971546699668.webp" />
        <Row className='content-container'>
          <Col className='content-body' xl={8}>
            <MDXRemote {...source} components={<p>Test</p>}/>
          </Col>
          <Col className={styles.contentMenu} xl={4}>
            <PostMenu postMenu={arrMenu} />
          </Col>
        </Row>
      </Container>
    </Layout>
  )
}

export const getStaticPaths = async () => {
  return {
    paths: [
      {params: {id: '1'}},
      {params: {id: '2'}}
    ],
    fallback: false
  }
}
export const getStaticProps = async () => {
  var content = "<h2 id='_Mo-bai-1'>Mở bài</h2>\
  <p>hãy sống theo cách của bạn</p>\
  <h2 id='_than-bai-2'>Thân bài</h2>\
  <h3 id='_test-3'>test</h3>\
  <p>bạn đang làm gì vậy?</p>\
  <h2 id='_ket-bai-4'>Kết bài</h2>\
  <p>Tìm cho mình niềm vui đi</p>"

  const arrMenu: PostMenuListType = getMenuPost(content)
  
  const posts = "---\ntitle: Home\n---\n```js\nconsole.log('Hello world')\n```\n# This is heading 1\n<h2 class='mt-4'>heading 2</h2>"
  const data = matter(posts)
  
  const mdxSource = await serialize(data.content, {
    // Optionally pass remark/rehype plugins
    mdxOptions: {
      remarkPlugins: [],
      rehypePlugins: [],
    },
    scope: data.data,
  })
  
  return {
    props: {
      arrMenu,
      posts: data.data,
      source: mdxSource
    }
  }
}
export default Post