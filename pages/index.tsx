import Layout from '../components/Layout'
import Container from 'react-bootstrap/Container'

import Famous from '../components/Famous'
import { PostType } from '../utils/Types'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import styled from 'styled-components'
import axios from 'axios'
import Post from '../components/Post'
import CarouselHomeBanner from '../components/CarouselHomeBanner'
import ListCategory from '../components/ListCategory'
import {useEffect, useState} from 'react'
import { ListFamousPost } from '../services/fetch/FamousPost'
import ListPanel from '../components/ListPanel'

const ListPostContainer = styled.div`
  display:flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: #f8ede8;
  padding: 40px 0 20px 0;
  color: #000;
  font-weight: bold;
  width: 100%;

  .list-posts {
    width: 100%;
  }
`
const listImageBanner = [
  {
    imgUrl: "/images/2015-11-11-global-shopping0-16050036844971546699668.webp",
    link: "tmp"
  },
  {
    imgUrl: "/images/big-sale-11-11-vcsr-2.jpg",
    link: "tmp"
  },
  {
    imgUrl: "/images/2015-11-11-global-shopping0-16050036844971546699668.webp",
    link: "tmp"
  }
]
export default function Home({posts}: {posts: Array<PostType>}) {
  const post: PostType = {
    id: 1,
    imgUrl: "/images/famous.jpg",
    postType: "Cầu lông",
    postTitle: "Vợt giá rẻ cho người mới tập chơi."
  }
  console.log(posts);
  
  return (
    <Layout>
      <Container className='pt-1' fluid="lg">
        <CarouselHomeBanner listItem={listImageBanner}/>
      </Container>
      <Container fluid="lg">
        <ListCategory />
        <Row className='mt-4 top-splitter'>
          <Col>News</Col>
        </Row>
        <Row  className="mt-3">
          <Col xl={8}>
            {post && <Famous post={post}/>}
          </Col>
          <Col>
            <ListPostContainer>
              <h2 className='home-heading'>Mới nhất</h2>
              <div className='list-posts'>
                {/* {posts?.map((item, index) => <Post key={index} post={item}/>)} */}
              </div>
            </ListPostContainer>
          </Col>
        </Row>
        <ListPanel/>
      </Container>
    </Layout>
  )
}

export const getStaticProps = async() => {
  const res = await axios.get("https://62135a53f43692c9c6015aed.mockapi.io/post", {
    headers: {
      Accept: 'application/json',
      'Accept-Encoding': 'identity'
    }
  })
  
  return {
    props: {
      posts: res.data
    }
  }
} 