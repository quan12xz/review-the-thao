import { NextApiRequest, NextApiResponse } from "next";
import Post from "../../../services/database/Post";
import connectMongoDB from "../../../services/database/connect";
import { ResponseType, Message } from "../../../services/common/Response";


export default function handler(req: NextApiRequest, res: NextApiResponse) {
    var objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
    connectMongoDB().catch(error => res.status(500).json(objResponse))
    const {method} = req

    switch (method) {
        case 'GET':
            Post.getPosts(req, res)
            break;
        case 'POST':
            Post.insertPost(req, res)
            break;
        case 'PUT':
            Post.updatePost(req, res)
            break;
        case 'DELETE': 
            Post.deletePost(req, res)
            break;
        default:
            objResponse = {success: false, message: Message.NOT_FOUND}
            res.status(400).json(objResponse)
            break;
    }
}