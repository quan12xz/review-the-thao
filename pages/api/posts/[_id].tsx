import { NextApiRequest, NextApiResponse } from "next";
import { Message, ResponseType } from "../../../services/common/Response";
import connectMongoDB from "../../../services/database/connect";
import Post from "../../../services/database/Post";

const handler = (req:NextApiRequest, res: NextApiResponse) => {
    var objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
    connectMongoDB().catch(()=> res.status(500).json(objResponse))
    const {method} = req
    switch (method) {
        case 'GET':
            Post.getPost(req,res)
            break;
        case 'POST':
            Post.insertPost(req, res)
            break
        case 'PUT':
            Post.updatePost(req, res)
            break
        case 'DELETE':
            Post.deletePost(req, res)
            break
        default:
            objResponse= {success: false, message: Message.NOT_FOUND}
            res.status(404).json(objResponse)
            break;
    }
}

export default handler