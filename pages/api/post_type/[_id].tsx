import { NextApiRequest, NextApiResponse } from "next"
import { Message, ResponseType } from "../../../services/common/Response"
import connectMongoDB from "../../../services/database/connect"
import PostType from "../../../services/database/PostType"

const handler = (req: NextApiRequest, res: NextApiResponse) => {
    var objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
    connectMongoDB().catch(()=> res.status(500).json(objResponse))
    const {method} = req
    switch (method) {
        case 'GET':
            PostType.getPostType(req, res)
            break;
        case 'POST':
            PostType.insertPostType(req, res)
            break
        case 'PUT':
            PostType.updatePostType(req, res)
            break
        case 'DELETE':
            PostType.deletePostType(req, res)
            break
        default:
            objResponse = {success: false, message: Message.INPUT_INVALID}
            res.status(400).json(objResponse)
            break;
    }
}

export default handler