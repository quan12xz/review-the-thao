import { NextApiRequest, NextApiResponse } from "next";
import PostType from "../../../services/database/PostType";
import connectMongoDB from "../../../services/database/connect";
import { ResponseType, Message } from "../../../services/common/Response";


export default function handler(req: NextApiRequest, res: NextApiResponse) {
    var objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
    connectMongoDB().catch(error => res.status(500).json(objResponse))
    const {method} = req

    switch (method) {
        case 'GET':
            PostType.getPostTypes(req, res)
            break;
        case 'POST':
            PostType.insertPostType(req, res)
            break;
        default:
            objResponse = {success: false, message: Message.EXCEPTION}
            res.status(400).json(objResponse)
            break;
    }
}