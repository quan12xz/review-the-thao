import React from 'react'
import { Container } from 'react-bootstrap'
import Layout from '../../../components/Layout'
import ListCategory from '../../../components/ListCategory'

const Study = () => {
  return (
    <Layout>
        <Container fluid="lg">
            <ListCategory/>
        </Container>
    </Layout>
  )
}

export default Study