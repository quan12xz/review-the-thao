import mongoose from "mongoose";

const connectMongoDB = async() => {
    try {
        const { connection } = await mongoose.connect(`mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PWD}@cluster0.xkcgown.mongodb.net/?retryWrites=true&w=majority`)

        if (connection.readyState === 1 ) {
            console.log("Database connected");
            
        }
    } catch (error) {
        return Promise.reject(error)
    }
}
export default connectMongoDB
