import { CallbackError } from "mongoose";
import { NextApiRequest, NextApiResponse } from "next";
import { ResponseType, Message } from "../common/Response";
import PostTypes from "../schemas/PostType";

class PostType {
    /**
     * GET /post_type?_id
     */
    getPostType = async(req:NextApiRequest, res: NextApiResponse) => {
        const {_id} = req.query
        if(!_id) {
            let objResponse: ResponseType = {success: false, message: Message.NOT_FOUND}
            return res.status(400).json(objResponse)
        }
        try {
            const postTypes = await PostTypes.find({_id})
            if(!postTypes) {
                let objResponse: ResponseType = {success: false, message: Message.NOT_FOUND}
                return res.status(404).json(objResponse)
            }
            let objResponse: ResponseType = {success: true, message: Message.SUCCESS, data: postTypes}
            res.status(302).json(objResponse)
        } catch (error) {
            let objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
            res.status(500).json(objResponse)
        }
    }

    /**
     * GET /post_type
     */
    getPostTypes = async(req:NextApiRequest, res: NextApiResponse) => {
        try {
            const postTypes = await PostTypes.find({})
            if(!postTypes) {
                let objResponse: ResponseType = {success: false, message: Message.NOT_FOUND}
                return res.status(404).json(objResponse)
            }
            let objResponse: ResponseType = {success: true, message: Message.SUCCESS, data: postTypes}
            res.json(objResponse)
        } catch (error) {
            let objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
            res.status(500).json(objResponse)
        }
    }

    /**
     * POST /post_type
     */
    insertPostType = async(req: NextApiRequest, res: NextApiResponse) => {
        const data = req.body
        if (!data) {
            let objResponse: ResponseType = {success: false, message: Message.INPUT_INVALID}
            return res.status(400).json(objResponse)
        }
        try {
            await PostTypes.create(data, (err: CallbackError, data: any) => {
                if(err) {
                    let objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
                    return res.status(500).json(objResponse)
                }
                let objResponse: ResponseType = {success: false, message: Message.SUCCESS, data}
                return res.status(201).json(objResponse)
            })
        } catch (error) {
            let objResponse: ResponseType = {success: false, message: Message.EXCEPTION, error}
            return res.status(500).json(objResponse)
        }
    }

    /**
     * PUT /post_type?_id
     */
    updatePostType = async(req:NextApiRequest, res:NextApiResponse) => {
        const data = req.body
        const {postTypeID} = req.query
        if(!data || !postTypeID) {
            let objResponse: ResponseType = {success: false, message: Message.INPUT_INVALID}
            return res.status(400).json(objResponse)
        }

        try {
            await PostTypes.findByIdAndUpdate(postTypeID, data, (err: CallbackError, data: any) => {
                if(err) {
                    let objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
                    return res.status(500).json(objResponse)
                }
                let objResponse: ResponseType = {success: false, message: Message.SUCCESS, data}
                return res.status(200).json(objResponse)
            })
        } catch (error) {
            let objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
            return res.status(500).json(objResponse)
        }
    }    

    /**
     * PUT /post_type?_id
     */
    deletePostType = async(req:NextApiRequest, res:NextApiResponse) => {
        const {postTypeID} = req.query
        if(!postTypeID) {
            let objResponse: ResponseType = {success: false, message: Message.INPUT_INVALID}
            return res.status(400).json(objResponse)
        }

        try {
            await PostTypes.findByIdAndDelete(postTypeID, (err: CallbackError, data: any) => {
                if(err) {
                    let objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
                    return res.status(500).json(objResponse)
                }
                let objResponse: ResponseType = {success: false, message: Message.SUCCESS, data}
                return res.status(200).json(objResponse)
            })
            res
        } catch (error) {
            let objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
            return res.status(500).json(objResponse)
        }
    }
    _checkInsertData = () => {

    }
}

export default new PostType()