import { CallbackError } from "mongoose";
import { NextApiRequest, NextApiResponse } from "next";
import { ResponseType, Message } from "../common/Response";
import Posts from "../schemas/Post";

class Post {
    /**
     * GET /post?_id
     */
    getPost = async(req:NextApiRequest, res: NextApiResponse) => {
        const {_id} = req.query
        if(!_id) {
            let objResponse: ResponseType = {success: false, message: Message.NOT_FOUND}
            return res.status(400).json(objResponse)
        }
        try {
            const posts = await Posts.find({_id})
            if(!posts) {
                let objResponse: ResponseType = {success: false, message: Message.NOT_FOUND}
                return res.status(404).json(objResponse)
            }
            let objResponse: ResponseType = {success: true, message: Message.SUCCESS, data: posts}
            res.json(objResponse)
        } catch (error) {
            let objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
            res.status(500).json(objResponse)
        }
    }

    /**
     * GET /post
     */
    getPosts = async(req:NextApiRequest, res: NextApiResponse) => {
        try {
            const posts = await Posts.find({})
            if(!posts) {
                let objResponse: ResponseType = {success: false, message: Message.NOT_FOUND}
                return res.status(404).json(objResponse)
            }
            let objResponse: ResponseType = {success: true, message: Message.SUCCESS, data: posts}
            res.json(objResponse)
        } catch (error) {
            let objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
            res.status(500).json(objResponse)
        }
    }

    /**
     * POST /post
     */
    insertPost = async(req: NextApiRequest, res: NextApiResponse) => {
        const data = req.body
        if (!data) {
            let objResponse: ResponseType = {success: false, message: Message.INPUT_INVALID}
            return res.status(400).json(objResponse)
        }
        try {
            await Posts.create(data, (err: CallbackError, data: any) => {
                if(err) {
                    let objResponse: ResponseType = {success: false, message: Message.EXCEPTION}
                    return res.status(406).json(objResponse)
                }
                let objResponse: ResponseType = {success: false, message: Message.SUCCESS, data}
                return res.status(201).json(objResponse)
            })
        } catch (error) {
            let objResponse: ResponseType = {success: false, message: Message.EXCEPTION, error}
            return res.status(500).json(objResponse)
        }
    }

    /**
     * PUT /post?_id
     */
    updatePost = async(req:NextApiRequest, res:NextApiResponse) => {
        const data = req.body
        const {_id} = req.query
        if(!data || !_id) {
            
            let objResponse: ResponseType = {success: false, message: Message.INPUT_INVALID}
            return res.status(400).json(objResponse)
        }

        try {
            
            await Posts.findByIdAndUpdate(_id, data)
            .then(data=> {
                let objResponse: ResponseType = {success: false, message: Message.SUCCESS, data}
                return res.status(500).json(objResponse)
            })
            .catch((error)=> {
                let objResponse: ResponseType = {success: false, message: Message.EXCEPTION, error: error}
                return res.status(400).json(objResponse)
            })
        } catch (error) {
            let objResponse: ResponseType = {success: false, message: Message.EXCEPTION, error: error}
            return res.status(500).json(objResponse)
        }
    }    

    /**
     * PUT /post?_id
     */
    deletePost = async(req:NextApiRequest, res:NextApiResponse) => {
        const {_id} = req.query
        if(!_id) {
            let objResponse: ResponseType = {success: false, message: Message.INPUT_INVALID}
            return res.status(400).json(objResponse)
        }

        try {
            await Posts.findByIdAndDelete(_id)
            .then((data) =>{
                let objResponse: ResponseType = {success: false, message: Message.SUCCESS, data}
                return res.status(200).json(objResponse)
            }).catch(error => {
                let objResponse: ResponseType = {success: false, message: Message.EXCEPTION, error}
                return res.status(400).json(objResponse)
            })
        } catch (error) {
            let objResponse: ResponseType = {success: false, message: Message.EXCEPTION, error}
            return res.status(500).json(objResponse)
        }
    }
    _checkInsertData = () => {

    }
}

export default new Post()