import axios from "axios"

export const ListFamousPost = async (page: number = 1) => {
    try {
        const res = await axios.get(`https://62135a53f43692c9c6015aed.mockapi.io/post?p=${page}&l=10`)
        return res.data
    } catch (error) {
        console.log(error)
    }
}