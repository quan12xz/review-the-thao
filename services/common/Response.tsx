export type ResponseType = {
    success: boolean,
    message: String,
    data?: Object,
    error?: any
}
export const Message = {
    NOT_FOUND: "Không tìm thấy dữ liệu",
    DUPLICATE: "Dữ liệu đã tồn tại",
    INPUT_INVALID: "Dữ liệu truyền vào không hợp lệ",
    EXCEPTION: "Có lỗi xảy ra",
    SUCCESS: "Thành công",
}