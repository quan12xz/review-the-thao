import { EmotionVotedList } from "../../utils/Types"

export const SortEmotionList = (emotionList: EmotionVotedList) => {
    return emotionList.sort((a ,b) => b[1] - a[1])
}

export const CountEmotions = (emotionList: EmotionVotedList) => {
    var count = 0;
    emotionList.forEach(item => count +=item[1])
    return count
}