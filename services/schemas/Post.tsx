import { Schema, models, model } from "mongoose";


const postSchema = new Schema({
    typeID: { type: Schema.Types.ObjectId, ref: 'post_type'},
    postTitle: { type: String, maxLength: 200},
    postContent: {type: String},
    createdDate: {type: Date, default: new Date()},
    updatedDate: {type: Date, default: new Date()},
    deletedDate: {type: Date, default: new Date()}
})

const Posts = models.post || model('post', postSchema)
export default Posts