import {Schema, models, model} from 'mongoose'

const postType = new Schema({
    name: {type: String, maxLength: 200},
    createdDate: {type: Date, default: new Date()},
    updatedDate: {type: Date, default: new Date()},
    deletedDate: {type: Date, default: new Date()}
})

const PostTypes = models.post_type || model('post_type', postType)
export default PostTypes